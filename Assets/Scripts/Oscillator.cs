﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class Oscillator : MonoBehaviour
{
    [SerializeField] Vector3 movementVector = new Vector3(20f, 0f, 0f);
    [SerializeField] float period = 2f;

    float movementFactor = 0f;

    Vector3 startingPos;

    // Start is called before the first frame update
    void Start()
    {
        startingPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        Oscillate();
    }

    private void Oscillate()
    {
        if(period <= Mathf.Epsilon) { return; } // if period is effectively 0, gtfo

        float cycles = Time.time / period;
        const float tau = Mathf.PI * 2f;
        float rawSinWave = Mathf.Sin(cycles * tau);  // sin moves between -1 and +1.
        movementFactor = rawSinWave / 2f + 0.5f; // adjusts sin to be between 0 and 1.
        Vector3 offset = movementVector * movementFactor;
        transform.position = startingPos + offset;
    }
}
