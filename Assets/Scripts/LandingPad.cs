﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LandingPad : MonoBehaviour
{
    [SerializeField] ParticleSystem successParticles;
    [SerializeField] AudioClip levelCompleteChime;
    [SerializeField] [Range(0.1f, 1f)] float chimeVolume = 1f;

    AudioSource audioSource;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }
    public void RunFinishSequence()
    {
        audioSource.PlayOneShot(levelCompleteChime, chimeVolume);
        successParticles.Play();
    }
}
