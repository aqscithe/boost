﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Level : MonoBehaviour
{
    [SerializeField] float levelLoadDelay = 3f;

    int sceneIndex;
    

    // Start is called before the first frame update
    void Start()
    {
        sceneIndex = SceneManager.GetActiveScene().buildIndex;
    }

    public IEnumerator LoadNextLevel()
    {
        yield return new WaitForSeconds(levelLoadDelay);
        SceneManager.LoadScene(sceneIndex + 1);
    }

    public IEnumerator LoadFirstLevel()
    {
        yield return new WaitForSeconds(levelLoadDelay);
        SceneManager.LoadScene("Level 1");
    }

    public bool LastLevel()
    {
        if(SceneManager.sceneCountInBuildSettings - sceneIndex == 1)
        {
            return true;
        }
        return false;
    }
}
