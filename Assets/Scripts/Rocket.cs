﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rocket : MonoBehaviour
{

    // config params
    [Header("Déplacement")]
    [SerializeField] float thrust = 1f;
    [SerializeField] float rotateSpeed = 1f;
    [Header("Audio")]
    [SerializeField] AudioClip rocketEngine;
    [SerializeField] [Range(0.1f, 1f)] float thrustVolume = 1f;
    [SerializeField] AudioClip rocketExplosion;
    [SerializeField] [Range(0.1f, 1f)] float explosionVolume = 1f;
    [Header("Particles")]
    [SerializeField] ParticleSystem explosionParticles;
    [SerializeField] ParticleSystem rocketJetParticles;

    // cached references
    Rigidbody myRigidBody;
    AudioSource myAudioSource;
    Level level;
    LandingPad landingPad;

    enum State { Alive, Dying, Transcending };
    State state = State.Alive;

    // Start is called before the first frame update
    void Start()
    {
        SetComponentReferences();
        SetObjectReferences();
    }

    private void SetObjectReferences()
    {
        level = FindObjectOfType<Level>();
        landingPad = FindObjectOfType<LandingPad>();
    }

    private void SetComponentReferences()
    {
        myRigidBody = GetComponent<Rigidbody>();
        myAudioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        ControlRocket();
    }

    private void ControlRocket()
    {
        if(state != State.Dying)
        {
            RespondToThrustInput();
            RespondToRotateInput();
        }
    }


    private void OnCollisionEnter(Collision collision)
    {
        if(state != State.Alive) { return; } // if dead or done w/ level ignore collisions

        switch(collision.gameObject.tag)
        {
            case "Friendly":
                Debug.Log("Amicable");
                break;
            case "Finish":
                ProcessFinish();
                break;
            default:
                ProcessDeath();
                break;
        }
    }

    private void ProcessDeath()
    {
        state = State.Dying;
        PlayDeathAudio();
        HandleParticlesOnDeath();
        StartCoroutine(level.LoadFirstLevel());
    }

    private void HandleParticlesOnDeath()
    {
        explosionParticles.Play();
        if (rocketJetParticles.isPlaying)
        {
            rocketJetParticles.Stop();
        }
    }

    private void PlayDeathAudio()
    {
        StopThrustAudio();
        myAudioSource.PlayOneShot(rocketExplosion, explosionVolume);
    }

    private void ProcessFinish()
    {
        state = State.Transcending;
        landingPad.RunFinishSequence();
        if (!level.LastLevel())
        {
            StartCoroutine(level.LoadNextLevel());
        }
        else
        {
            StartCoroutine(level.LoadFirstLevel());
        }
    }

    private void RespondToRotateInput()
    {
        myRigidBody.freezeRotation = true; //stop physics from affecting object rotation

        if (Input.GetAxis("Horizontal") < 0)
        {
            transform.Rotate(new Vector3(0f, 0f, rotateSpeed * Time.deltaTime));
        }
        else if (Input.GetAxis("Horizontal") > 0)
        {
            transform.Rotate(new Vector3(0f, 0f, -rotateSpeed * Time.deltaTime));
        }

        myRigidBody.freezeRotation = false; // return physics control over rotation
    }

    private void RespondToThrustInput()
    {
        if (Input.GetButton("Boost"))
        {
            PlayThrustAudio();
            ApplyThrust();
            rocketJetParticles.Play();
            
        }
        else
        {
            StopThrustAudio();
            rocketJetParticles.Stop();
        }
    }

    private void ApplyThrust()
    {
        myRigidBody.AddRelativeForce(Vector3.up * thrust * Time.deltaTime);
    }

    private void PlayThrustAudio()
    {
        if (!myAudioSource.isPlaying) // so the audio doesn't layer
        {
            myAudioSource.PlayOneShot(rocketEngine, thrustVolume);
        }
    }

    private void StopThrustAudio()
    {
        myAudioSource.Stop();
    }
}
